#!/bin/bash
set -e

CMD=("$@")

CONTAINER_NAME=$(curl -sf http://rancher-metadata/2015-12-19/self/container/name)
STACK_NAME=$(curl -sf http://rancher-metadata/2015-12-19/self/stack/name)
SERVICE_NAME=$(curl -sf http://rancher-metadata/2015-12-19/self/service/name)


if [ -z "$STACK_NAME" ]; then
    echo "Stack name is empty"
    exit 1
fi

if [ -z "$SERVICE_NAME" ]; then
    echo "Service name is empty"
    exit 1
fi

for c in $(curl -sf "http://rancher-metadata/2015-12-19/stacks/${STACK_NAME}/services/${SERVICE_NAME}/containers"); do
    i=$(echo $c | cut -d '=' -f1)
    NAME=$(curl -sf "http://rancher-metadata/2015-12-19/stacks/${STACK_NAME}/services/${SERVICE_NAME}/containers/${i}/name")
    if [ ! -z "$NAME" ] && [ "$CONTAINER_NAME" != "$NAME" ]; then
        echo "Joining cluster with ${NAME}"
        CMD+=("--join" "${NAME}:29015")
    fi
done

echo "Running command: ${CMD[@]}"
exec "${CMD[@]}"