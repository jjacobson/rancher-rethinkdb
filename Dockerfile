FROM library/rethinkdb:2.3

RUN apt-get update \
    && apt-get install -y curl --no-install-recommends

COPY docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["rethinkdb", "--bind", "all"]
